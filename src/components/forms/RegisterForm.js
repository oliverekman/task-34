import React, { useState } from 'react';

const RegisterForm = props => {


    const [ username, setUsername ] = useState('');
    const [ password, setPassword ] = useState('');


    const onRegisterClicked = ev => {
        console.log('EVENT: ', ev.target);

        //HttpRequest.
        //Done
        props.click({
            success: true,
            message: 'Register successfully'
        });
    }

    const onUsernameChange = ev => setUsername(ev.target.value);
    const onPasswordChange = ev => setPassword(ev.target.value);

    return (
        <form>
            <div>
                <label>Username: </label>
                <input type="text" placeholder="Enter a username" onChange={ onUsernameChange } ></input>
            </div>
            <div>
                <label>Password: </label>
                <input type="password" placeholder="Enter a password" onChange={ onPasswordChange } ></input>
            </div>

            <div>
                <button type="button" onClick={ onRegisterClicked }>Register</button>
            </div>
        </form>
    )
}

export default RegisterForm;

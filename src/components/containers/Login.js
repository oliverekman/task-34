import React from 'react';
import LoginForm from '../forms/LoginForm';

const Login = () => {
    return (
        <div>
            <h1>Login to Survey</h1>

            <LoginForm></LoginForm>
        </div>
    )
};

export default Login;